<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\Auth\AuthController;
use App\Http\Controllers\API\v1\Post\PhotoController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1/user')->controller(AuthController::class)->group(function () {
    Route::post('/login', 'login');
    Route::get('/me', 'me');
});
Route::prefix('v1/photos')->controller(PhotoController::class)->group(function () {
    Route::post('/', 'upload');
    Route::get('/', 'index');
    Route::get('/{id}', 'detail');
    Route::put('/{id}/update', 'update');
    Route::delete('/{id}/delete', 'delete');
    Route::post('/{id}/like', 'like');
    Route::post('/{id}/unlike', 'dislike');
    // Route::get('/me', 'me');
});