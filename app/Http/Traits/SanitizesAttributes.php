<?php

namespace App\Traits;

trait SanitizesAttributes
{
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        
        $excludedAttributes = ['email', 'password', 'otp'];

        if (is_string($value) && !in_array($key, $excludedAttributes, true)) {
            return htmlentities($value, ENT_QUOTES | ENT_HTML5, 'UTF-8');
        }

        return $value;
    }
}