<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    /**
     * Respond Success API
     * 
     * @param $data (array), $message (string), $statusCode (int)
     * @return json
     */

    public function RespondSuccess($data = [], $message = 'SUCCESS', $statusCode = 200)
    {
        $data = preg_replace('/\bnull\b/', '""', json_encode($data));

        return $this->Respond(array(
            'DATA' => json_decode($data) == "" ? (object)[] : json_decode($data),
            'MESSAGE' => $message,
            'STATUS' =>  $statusCode,
        ), 200);
    }

    /**
     * Respond Error API
     * 
     * @param $message (string), $statusCode (int)
     * @return json
     */

    public function RespondError($message = 'ERROR', $statusCode = 500, $data = [])
    {
        return $this->Respond(array(
            'DATA' => $data,
            'MESSAGE' => $message,
            'STATUS' =>  $statusCode,
        ), 200);
    }
    /**
     * Respond Validation Error API
     * 
     * @param $message (string), $statusCode (int)
     * @return json
     */

    public function RespondErrorValidator($message = 'Validation Error', $statusCode = 500, $data = [])
    {
        $message = (array) $message;
        $message = array_flatten(array_splice($message, 0, -1));
        return $this->Respond(array(
            'DATA' => $data,
            'MESSAGE' => $message[0],
            'STATUS' =>  $statusCode,
        ), 200);
    }
    /**
     * Respond API
     * 
     * @param $data (array), $statusCode (int), $headers (array)
     * @return json
     */

    public function Respond($data, $statusCode, $headers = array())
    {
        return response()->json($data, $statusCode, $headers, JSON_PRETTY_PRINT);
    }
    public function uploadFile($userId, $uploadedFile, $dir)
    {
        if (!is_dir('public/media/' . $dir . '/' . $userId . '/')) {
            @mkdir('public/media/' . $dir . '/' . $userId . '/', 0777, true);
        }
        $path = base_path() . '/public/media/' . $dir . $userId;
        $file = $uploadedFile;
        $name = $uploadedFile->getClientOriginalName();
        $newName = rand(1, 1000000000) . '_' . $name;
        $res = '/media/' . $dir . $userId . '/' . $newName;
        $file->move($path, $newName);
        return  $res;
    }
    public function removeFile($filePath)
    {
        $publicPath = public_path();
        $absolutePath = $publicPath . '/' . $filePath;
        if (file_exists($absolutePath)) {
            return unlink($absolutePath);
        }
        return false;
    }
}
