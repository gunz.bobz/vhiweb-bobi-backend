<?php
 
namespace App\Http\Controllers\API\v1\Auth;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Hash ;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    protected $jwt;
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->middleware('jwt.api', [
            'except' => ['login']
        ]);
        $this->jwt = $jwt;
    }
    /**
     * user Login
     * bobi 2023/06/12
     * @param form-data/JSON-RAW
     * @return JSON
     */ 
    public function login(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',

        ]);
        if ($validator->fails()) {
            return $this->RespondError('Email or/and password required.', 400, (object)[]);
        }

        // $input = $request->only('email', 'password');
        // $jwt_token = null;
  
        // if (!$jwt_token = JWTAuth::attempt($input)) {
        //     return $this->RespondError("Email or/and password doesn't match.", 400, (object)[]);
        // }
  
        // return response()->json([
        //     'success' => true,
        //     'token' => $jwt_token,
        // ]);

        try {
            $user = User::where('email', $request->email)->first();
            if (!$user){
                return $this->RespondError('Email Belum terdaftar!', 400, (object)[]);
            }
            if (!Hash::check($request->password, $user->password)) {
                return $this->RespondError("Email or/and password doesn't match.", 400, (object)[]);
            }
            try {

                $customClaims = [
                    'User' => $user,
                ];
                // GENERATE TOKEN
                $token = $this->jwt->fromUser($user, $customClaims);
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return $this->RespondError($e->getMessage(), 400, (object)[]);
            }
            $request->headers->set('Authorization', 'Bearer ' . $token);
            $kom =  collect($user)->toArray();
         
            $userData['User'] = $kom;
            $userData['token'] = $token;

            return $this->RespondSuccess($userData);
        } catch (\Exception $exception) {
            return $this->RespondError((object)[], $exception->getMessage());
        }

    }
    public function me(Request $request)
    {
        $payLoad = $this->jwt->parseToken()->getPayload();
        $data = User::where('id',$payLoad['data'])->first();
        $customClaims = [
            'member'=>$data,
        ];
        // GENERATE TOKEN
        $token = $this->jwt->fromUser($data, $customClaims);
        $member['customer'] = collect($data)->toArray();
        $member['token'] =$token;
    
        return $this->RespondSuccess($member);
    }
 
 
}