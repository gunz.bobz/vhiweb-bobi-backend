<?php

namespace App\Http\Controllers\API\v1\Post;

use App\Models\Image;
use App\Models\ImageTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\JWTAuth;
use Validator;
use DB;

class PhotoController extends Controller
{
    protected $jwt;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(JWTAuth $jwt)
    {
        $this->middleware('jwt.api', [
            'except' => ['detail']
        ]);
        $this->jwt = $jwt;
    }


    public function upload(Request $request)
    {
        $payLoad = $this->jwt->parseToken()->getPayload();
        $fileName = null;
        $validator = Validator::make($request->all(), [
            'caption' => 'required',
            'file' => 'required|mimes:jpeg,png,gif,bmp',
        ]);

        $firstError = $validator->errors()->first();

        if ($validator->fails()) {
            return $this->RespondError($firstError, 400, (object)[]);
        }

        // Upload the file
        if ($request->hasFile('file')) {
            $uploadedFile = $request->file('file');
            $fileName = $this->uploadFile((string)$payLoad['data'], $uploadedFile, 'Image');
        }

        if ($fileName != null) {
            // Create a new image record in the database
            $insertImage = Image::create([
                'file' => $fileName,
                'caption' => $request->caption,
                'user_id' => $payLoad['data'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            if ($insertImage) {
                // Update image tags if provided
                if ($request->filled('tags')) {
                    $inpt = $request->tags;
                    $inptArray = explode(', ', $inpt);
                    $imageTags = [];

                    // Delete existing image tags
                    ImageTag::where('image_id', $insertImage->id)->delete();

                    // Create new image tag records
                    foreach ($inptArray as $value) {
                        $imageTags[] = [
                            'image_id' => $insertImage->id,
                            'tag' => $value,
                        ];
                    }

                    if (!empty($imageTags)) {
                        ImageTag::insert($imageTags);
                    }
                }

                return $this->RespondSuccess($insertImage);
            } else {
                return $this->RespondError('Failed to insert into the database', 400, (object)[]);
            }
        } else {
            return $this->RespondError('Failed to upload the image', 400, (object)[]);
        }
    }

    public function detail(Request $request, $id)
    {
        $loggedIn = false;

        // Check if the request contains an authorization header
        if ($request->header('Authorization')) {
            $payLoad = $this->jwt->parseToken()->getPayload();
            $loggedIn = $payLoad['data'];
        }

        $images  = new Image();

        // Retrieve detailed information about the image
        $result = $images->detailPhotos($loggedIn, $id);

        return $this->RespondSuccess($result);
    }

    public function index(Request $request)
    {
        $loggedIn = false;

        // Check if the request contains an authorization header
        if ($request->header('Authorization')) {
            $payLoad = $this->jwt->parseToken()->getPayload();
            $loggedIn = $payLoad['data'];
        }

        $images  = new Image();

        // Retrieve all photos, including additional details based on user login status
        $result = $images->allPhotos($loggedIn);

        return $this->RespondSuccess($result);
    }
    public function update(Request $request, $id)
    {
        $param = $request->only(['caption', 'tags']);

        // Check if the 'caption' field is filled in the request
        if ($request->filled('caption')) {
            // Update the 'caption' column of the Image model with the provided value
            Image::where('id', $id)->update([
                'caption' => $param['caption']
            ]);
        }

        // Check if the 'tags' field is filled in the request
        if ($request->filled('tags')) {
            $inpt = $request->tags;
            $inptArray = explode(', ', $inpt);
            $imageTags = [];

            // Delete existing ImageTag records associated with the given image ID
            ImageTag::where('image_id', $id)->delete();

            // Iterate over the 'tags' array and create new ImageTag records
            foreach ($inptArray as $value) {
                $imageTags[] = [
                    'image_id' => $id,
                    'tag' => $value,
                ];
            }

            // Insert the new ImageTag records into the database
            if (!empty($imageTags)) {
                ImageTag::insert($imageTags);
            }
        }

        // Retrieve the updated Image record
        $res = Image::find($id);

        return $this->RespondSuccess($res);
    }

    public function delete($id)
    {
        $payLoad = $this->jwt->parseToken()->getPayload();
        $userId = $payLoad['data'];
    
        // Select the 'file' column from the 'images' table where ID and user ID match
        $res = \DB::table('images')->select('file')->where('id', $id)->where('user_id', $userId)->first();
    
        if ($res) {
            // Remove the file associated with the image
            $deleteFile = $this->removeFile($res->file);
    
            if ($deleteFile) {
                // Delete the Image record from the database
                Image::where('id', $id)->delete();
                return $this->RespondSuccess((object)[]);
            } else {
                return $this->RespondError('failed to DeleteFIle', 400, (object)[]);
            }
        } else {
            return $this->RespondError('not found', 400, (object)[]);
        }
    }
    
    public function like(Request $request, $id)
    {
        $check = \DB::table('images')->select('file')->where('id', $id)->first();
        if ($check) {
            // Get the user ID from the JWT payload
            $payLoad = $this->jwt->parseToken()->getPayload();
            $userId = $payLoad['data'];
    
            // Update or insert a record in the 'image_likes' table
            DB::table('image_likes')
                ->updateOrInsert(
                    ['image_id' => $id, 'user_id' => $userId],
                    ['is_like' => 1]
                );
    
            $loggedIn = false;
            if ($request->header('Authorization')) {
                // Check if the user is logged in
                $payLoad = $this->jwt->parseToken()->getPayload();
                $loggedIn = $payLoad['data'];
            }
    
            $images  = new Image();
            // Retrieve the updated details of the image
            $result = $images->detailPhotos($loggedIn, $id);
            return $this->RespondSuccess($result);
        } else {
            return $this->RespondError('image not found', 400, (object)[]);
        }
    }
    
    public function dislike(Request $request, $id)
    {
        $check = \DB::table('images')->select('file')->where('id', $id)->first();
        if ($check) {
            // Get the user ID from the JWT payload
            $payLoad = $this->jwt->parseToken()->getPayload();
            $userId = $payLoad['data'];
    
            // Update or insert a record in the 'image_likes' table
            DB::table('image_likes')
                ->updateOrInsert(
                    ['image_id' => $id, 'user_id' => $userId],
                    ['is_like' => 2]
                );
    
            $loggedIn = false;
            if ($request->header('Authorization')) {
                // Check if the user is logged in
                $payLoad = $this->jwt->parseToken()->getPayload();
                $loggedIn = $payLoad['data'];
            }
    
            $images  = new Image();
            // Retrieve the updated details of the image
            $result = $images->detailPhotos($loggedIn, $id);
            return $this->RespondSuccess($result);
        } else {
            return $this->RespondError('image not found', 400, (object)[]);
        }
    }
    
}
