<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class ImageLike extends Model
{
    protected $table        = 'image_like';
    protected $primaryKey   = 'id';
    // public $timestamps = false;


    /**
     * The attributes that should be guarded for mass insert.
     *
     * @var array
     */
    protected $guarded = ['id'];

  

}
