<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table        = 'users';
    protected $primaryKey   = 'id';
    // Rest omitted for brevity
    protected $hidden = ['pivot', 'password'];

    /**
     * The attributes that should be guarded for mass insert.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $events = [];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'authType' => 'user',
            'data' => $this->id,
            // 'data' => [
            //     'memberId' => $this->member_id,
            // ]
        ];
    }
    
}