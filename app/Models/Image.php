<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Image extends Model
{
    // use SanitizesAttributes;
    protected $table        = 'images';
    protected $primaryKey   = 'id';
    // public $timestamps = false;


    /**
     * The attributes that should be guarded for mass insert.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $appends = [
        'tags',
        'like_count',
        'dislike_count'
    ];

    function getFileAttribute($val){
        return url('/').$val;
    }
    public function AllPhotos($isLoggedIn)
    {
        if ($isLoggedIn == 0) {
            return $this->selectRaw('*, 0 as is_liked')->get();
        } else {
            return $this->leftJoin('image_likes', function ($join) use ($isLoggedIn) {
                    $join->on('images.id', '=', 'image_likes.image_id')
                         ->where('image_likes.user_id', '=', $isLoggedIn);
                })
                ->selectRaw('images.*, IFNULL(image_likes.is_like, 0) as is_liked')
                ->get();
        }
    }
    public function detailPhotos($isLoggedIn, $id)
    {
        $query = $this->select('*');
    
        if ($isLoggedIn != 0) {
            $query->leftJoin('image_likes', function ($join) use ($isLoggedIn) {
                    $join->on('images.id', '=', 'image_likes.image_id')
                         ->where('image_likes.user_id', '=', $isLoggedIn);
                })
                ->addSelect(\DB::raw('IFNULL(image_likes.is_like, 0) as is_like'));
        } else {
            $query->addSelect(\DB::raw('0 as is_like'));
        }
    
        return $query->where('id', $id)->first();
    }
    
    function getTagsAttribute(){
        $tag = \DB::table('image_tags')->select('tag')->where('image_id',$this->id)->pluck('tag');
        return $tag;
    }
    function getLikeCountAttribute(){
        $tag = \DB::table('image_likes')->where('image_id',$this->id)->where('is_like',1)->count();
        return $tag;
    }
    function getDislikeCountAttribute(){
        $tag = \DB::table('image_likes')->where('image_id',$this->id)->where('is_like',2)->count();
        return $tag;
    }

}
